## Jay's Board Game Table (Games)

This repository houses the table settings and the config files for various games that run on my board game table. More information about the project can be found here: https://www.instructables.com/Board-Game-Table-With-End-Turn-Buttons/ 
     
The actual code used to run the table itself can be found in this repository: https://gitlab.com/jason.watson/board-game-table
     
View more of my projects here: https://www.claireandjay.ca 